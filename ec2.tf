# EC2 instance

resource "aws_instance" "web" {
  instance_type = "t3.micro"
  ami = "ami-0b850cf02cc00fdc8"
  key_name = aws_key_pair.gitlab.id
  vpc_security_group_ids = [aws_security_group.main_sg.id]

  tags = {
    Name = "gitops-demo"
    env = var.env
  }
}

# SSH key

resource "aws_key_pair" "gitlab" {
  key_name_prefix = "gitlab-gitops-demo-"
  public_key = chomp(tls_private_key.gitlab.public_key_openssh)
}

resource "tls_private_key" "gitlab" {
  algorithm = "RSA"
  rsa_bits = "2048"
}

resource "local_file" "ec2_private_key" {
  content = tls_private_key.gitlab.private_key_pem
  filename = "${path.module}/ec2.pem"
}

# Generate dotenv artifact for GitLab

resource "local_file" "dotenv" {
  content = "EC2_EXTERNAL_IP=${aws_instance.web.public_ip}"
  filename = "${path.module}/deploy.env"
}

# Generate EC2 inventory for Ansible

resource "local_file" "ansible_inventory" {
  content = aws_instance.web.public_ip
  filename = "${path.module}/hosts.ini"
}

resource "aws_s3_bucket" "demo" {
  bucket_prefix = "gitlab-iac-demo-s3-"
  acl = "public-read"
}

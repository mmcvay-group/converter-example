const fs = require('fs')
const getUuid = require('uuid-by-string')

fs.readFile('tfsec-output.json', 'utf8', function (err, data) {
  if (err) {
    return console.log(err)
  }
  
  if (data.charCodeAt(0) != 123) {
    // strip out tfsec warning message(s)
    data = data.substring(data.indexOf("{"));
  }

  const tfsecObj = JSON.parse(data);

  let sastObj = {
    version: "2.0",
    vulnerabilities: tfsecObj.results.map((result) => {
      return {
        id: getUuid(JSON.stringify(result)),
        category: "sast",
        name: result.description,
        message: result.description,
        description: result.description,
        severity: (
          (result.severity == "INFO") ? "Info" :
          (result.severity == "WARNING") ? "Critical" :
          (result.severity == "ERROR") ? "High" :
          "Unknown"
        ),
        confidence: "High",
        scanner: {
          id: "tfsec",
          name: "tfsec",
          url: "https://github.com/tfsec/tfsec",
          version: "1.0",
          vendor: {
            name: "tfsec"
          }
        },
        location: {
          file: result.location.filename.substring(process.cwd().length + 1),
          start_line: result.location.start_line,
          end_line: result.location.end_line,
          dependency: {
            package: {}
          }
        },
        identifiers: [{
          type: "tfsec",
          name: result.rule_id,
          value: result.rule_id,
          url: result.links.slice()
        }]
      }
    }),
    remediations: []
  }

  // Generate the SAST artifact.
  fs.writeFile('gl-sast-report.json', JSON.stringify(sastObj), function (err) {
    if (err) {
      return console.log(err)
    }

    console.log('SAST artifact from `tfsec` saved to gl-sast-report.json')
  })
})

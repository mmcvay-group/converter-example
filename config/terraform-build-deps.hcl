terraform {
  version = "0.14.4"
}

providers {
  aws = {
    versions = ["~> 3.0"]
  }

  local = {
    versions = ["~> 2.0"]
  }

  template = {
    versions = ["~> 2.0"]
  }

  tls = {
    versions = ["~> 3.0"]
  }
}
